const Parser = require("rss-parser");
const fs = require("fs");
let parser = new Parser();

module.exports = async function() {
  let feed = await parser.parseURL("https://www.robotscooking.com/rss/");
  // to check things
  // fs.writeFile("test.json", JSON.stringify(feed), (err) => {
  //   if (err) throw err;
  // });
  //

  const chosenarticles = feed.items
    .filter((a) => a.content?.length > 5)
    .slice(0, 3);

  return chosenarticles;
};
