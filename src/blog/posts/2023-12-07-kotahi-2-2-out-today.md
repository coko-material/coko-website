---
published: true
title: Kotahi 2.2 out today!
date: 2023-12-06
intro: >
  Kotahi 2.2 brings customizable PDFs via Pagedjs editor, editable email
  templates & variables, expanded language support w/ i18next, COAR Notify
  integration and more! Tailor workflows & extend global reach!
icon: /static/images/uploads/production_editor_njk.png
image: /static/images/uploads/production_editor_njk.png
---
## Release v2.2.0 (2.2.0)

### Downloads

- [Source code 2.2.0 (zip)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/2.2.0/kotahi-2.2.0.zip)
- [Source code 2.2.0 (tar.gz)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/2.2.0/kotahi-2.2.0.tar.gz)
- [Source code 2.2.0 (tar.bz2)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/2.2.0/kotahi-2.2.0.tar.bz2)
- [Source code 2.2.0 (tar)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/2.2.0/kotahi-2.2.0.tar)

**Pagedjs CSS editor** has been added to the Production editor. This allows users with access to the Production editor to edit the HTML and CSS templates. These 
changes will only be reflected in the PDF. User documentation will be provided in the [Kotahi User guide](https://kotahi.community/docs/) shortly.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/258c07054880c75f6b2c74ce378f5e42/Production_editor_njk.png)

**Create & edit Email notifications** Group Managers can create a new template or edit an existing template from the Settings>Email page.

Email body content can be personalised using the Handlebars.js variables framework. We have created a set of variables that map to select metadata content that is either system-generated (e.g. manuscript id) or captured in a form (e.g. manuscript title).

These initial variables allow for the insertion of text and hyperlinks in the body content of an email notification template e.g. Manuscript titles, sender/recipient usernames, manuscript ids etc.

Copy/paste the variable and curly bracket into the body content of your email as desired.

- Manuscript title - {{ manuscriptTitle }}
- Group login link - {{{ loginLink }}}
- Sender name - {{ senderName }}
- Recipient name - {{ recipientName }}
- Author name - {{ authorName }}
- *Manuscript link (URL) - {{{ manuscriptLink }}}
- Manuscript title link (`submission.link`) - {{{ manuscriptTitleLink }}}
- Manuscript number (`shortId`) - {{ manuscriptNumber }}

*This link is adapted based on the receiver’s role. If an editor receives an email notification that includes a link to a Manuscript in Kotahi - the link will point to the Control panel. If the recipient is a reviewer, the link will direct the user to the Review page and the author to the Submission page.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/712a198e6a0905680f08a5f2fc9ea714/email_template.png)

`NOTIFICATION_EMAIL_CC_ENABLED=` variable to enable/disable carbon copies (CC) has now been deprecated. Group Managers can manually add a CC from the Emails>Email template>CC field - see [CHANGES.md](http://CHANGES.md)

**COAR Notify integration** Kotahi can receive messages from [COAR’s Notify service](https://www.coar-repositories.org/notify/). Authors can submit a manuscript to a 3rd party server and request a review from a group in Kotahi.

A request results in a manuscript being imported and displayed on the Manuscripts page. Manuscripts imported via COAR Notify are identifiable by the Notify logo in the title text.

Only the SciELO server supports these author requests. The development of a similar solution is in progress on bioRxiv.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/81096e444f49f8fbdf64e7408af6643c/coarnotify_example.png)

**Discussion (Chat) channels are now collapsable** maximising the use of page real estate and minimising clutter, particularly on pages like the Control panel.

An indicator and count makes it easier to identify discussions containing new or unread posts.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/3bc6a1db639207ed7bfe45aaf896434b/chat_expanded.png)

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/a8a0ad7b2c0c7bf1042a077b38e9e3f8/chat_collapsed.png)

**Task description field** has been added to the Task  builder and editor modals. A tooltip has also been added to the title for legibility, particularly for small screen sizes when the Discussion viewport is expanded.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/1e4fa933a881f634336b65f8b566dc8d/task_title_tooltip.png)

**User interface language translation** is now possible through an implementation of [i18next](https://www.i18next.com/). The majority of the workflow-related user interface component language strings are now accessible for translation via the backend. Thanks to folks at Alexandrina for making this contribution.

We have used AI to automatically translate a selection of sample languages for demonstration purposes - as a user you can select your preferred language from the dropdown menu on the Profile page.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/81833da3dad776e45b1c39f5f60cc35e/Language_selection.png)

**Bug fixes;**

- Performance fix for XSweet to support conversion of large manuscript files.
- Fix for handling of inline equation conversions and their display in Wax.
- Adjustment to the upload logic pulls out `figure` tags placed inside of a `p` or a `h1-6`, and wraps them in a sibling element.
- The height of the Wax editor viewport for editing page content in the CMS>Pages.
- Fix for unsafe loops in code that were starting tasks without `await` to catch possible exceptions. This could have resulted in server crash.
- Bypass periodic database queries to allow the database to go idle 
during periods of inactivity. This is to reduce database hosting costs.

**Next release;**

- Author-proofing workflow MVP.
- Updates to the form builder field nomenclature, adding of specific data types and support for multiple submission forms.
- Updates to the CMS to add group logos and favicons.
- Update to archetype nomenclature.




