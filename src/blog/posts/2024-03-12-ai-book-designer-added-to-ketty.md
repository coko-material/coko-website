---
published: true
title: Ai Book Designer added to Ketty!
date: 2024-03-13
intro: |
  We are very happy to announce the release of the AI Book Designer in Ketty!
video: https://vimeo.com/922659787
icon: /static/images/uploads/aiketty.png
---
For more information please see - [https://mailchi.mp/a35f26bedaae/coko-newsletter-8308164](https://mailchi.mp/a35f26bedaae/coko-newsletter-8308164)
