---
title: "Nvcleus by Amnet: Harnessing Coko's Kotahi for a Cost-Effective Journal
  Solution"
date: 2023-04-13
published: true
intro: Amnet and Coko are celebrating the launch of Nvcleus, a cost-effective
  journal solution based on the Kotahi platform, at the London Book Fair. This
  partnership has successfully developed an innovative approach to journal
  publishing, focusing on affordability and openness for publishers.
video: ""
icon: /static/images/uploads/cropped-nvcleus_oval-162x54.png
---
Coko has been working closely with Amnet for the past two years, and we are delighted to see the fruits of our collaboration. Amnet is now ready to launch their new journal solution, Nvcleus, which is based on our Kotahi platform. This marks a significant milestone for Coko, and we are excited to see the positive impact it will have on the journal publishing industry.

At Coko, we have intentionally decided not to offer hosting services. Instead, we focus on building partnerships with hosting and service providers, like Amnet. This approach allows us to keep the cost of our software down for two main reasons: firstly, we don't rely on revenue from hosting; and secondly, we can introduce partnerships with multiple organizations to ensure competitive pricing. This strategy has been particularly important for Kotahi, as the journal sector has long been burdened by unnecessarily high costs due to exorbitant vendor pricing. We strongly believe that software should be open and as affordable as possible for end users, such as publishers.

We are thrilled to see Amnet launching Nvcleus at the London Book Fair this coming week. Our partnership has been enjoyable and fruitful over the past two years, and we eagerly anticipate many more years of collaboration. We wish Amnet the best of luck at the London Book Fair and would like to take this opportunity to express our appreciation for their dedication and hard work. They are truly wonderful partners, and we are confident that their success will make a meaningful difference in the world of journal publishing. Good luck at the LBF, Amnet!

https://nvcleus.com/
