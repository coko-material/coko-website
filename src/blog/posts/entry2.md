---
published: true
title: "Announcement: Women in Tech Collectives and Coko form Partnership" 
date: 2021-11-01
intro: Coko and the Women in Tech Collectives are proud to announce a new partnership! The Women in Tech Collectives from India is a small community led by women tech enthusiasts…
author: Adam Hyde
tags: featured
class: 
icon: "/images/uploads/WITc.png"
---

![wit](/images/uploads/WITc.png)

Coko and the Women in Tech Collectives are proud to announce a new partnership!

The Women in Tech Collectives from India is a small community led by women tech enthusiasts from a small rural region in South India. The community helps in addressing the needs of employment and education for women and marginalized communities in this most needful time and supporting them to pursue careers in varied technology fields.

The members from the community usually come from rural regions and they are usually the first graduates in their families, and their access to the digital world is very limited. Also, many women in the community also face a lot of gender bias in accessing technology and in pursuing their career in Information and Communication Technologies. Their employment circumstances have also been severely (negatively) effected by COVID.

The Women In Tech collectives, India is a Self Help community which was initiated by [Bhuvana Meenakshi Koteeswaran](https://www.linkedin.com/in/bhuvana-meenakshi-koteeswaran-a67b58103) in March 2021 when a team of technology geeks, mostly women from these rural regions, reported about their sudden unemployment issues in the course of the pandemic. They were working for some local companies in the region as their families never allowed them to travel far to work. This community had to face social stigma to pursue their careers in tech and also continue their education on computers. Many women anonymously reported their concerns, because their families depended on their salaries, and staying at home increased their daily chores, so they were in need of some kind of job that can satisfy their daily essentials and medical emergencies. That’s when we formed the team and started to send word by mouth and WhatsApp messages to gather around those who are facing these issues.

In June of 2021 Bhuvana approached Coko Founder Adam Hyde about possible collaborations which have lead to a new partnership. Coko has employed four of the WiTc to work on open source publishing projects including Kotahi and Editoria.

> ”_I am very grateful about the partnership with Coko! I see this as a win-win state. It’s an amazing opportunity for our diverse community to get a global exposure which they have never experienced before. Thanks to Adam and his team for accommodating our community members so well and I look forward to sustaining this association for more years to come._” – Bhuvana Meenakshi, Founder of WITc, India

Coko Founder Adam Hyde is also very happy about this collaboration – “Bhuvana and the WITc are awesome! We have welcomed 4 people from the collective into our team so far and looking to work with more within a few weeks. Everyone has been extremely nice, hard working, and enthusiastic about what we do. “

Coko is also supporting these four team members to work during the week on other WiTc projects to assist the community. Additionally Bhuvana has joined the judging panel of the Open Publishing Awards and Adam and Bhuvana have more exciting ideas for WItC and Coko collaborations in the pipeline to announce! Stay tuned!
