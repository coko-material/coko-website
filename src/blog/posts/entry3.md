---
published: true
title: "Adam Hyde recieves Open Source Award"
date: 2022-02-02
intro: Coko Founder Adam Hyde recieves Open Source Award for "Exceptional Leadership"
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/nzosa-logo-large.png"
---

![nzosa](/images/uploads/nzosa-logo-large.png)

Coko Founder Adam Hyde was presented with a [New Zealand Open Source Award](https://nzosa.org.nz/) yesterday for "Exceptional FOSS leadership".

Adam is originally from New Zealand but has been living outside the country until recently. Since 2007 Adam has founded many open source projects. In 2020 Adam returned to NZ to shelter from the pandemic in the small surf town of Raglan. From there Adam lives, surfs, and runs Coko.

"I am super honored to recieve this award. I have been away from NZ for a long time and I didn't actually think folks here knew much of my work. So to get this is a real honor. I guess when the home team gives you a pat on the back it somehow feels very special"
-- Adam Hyde, Coko Founder
