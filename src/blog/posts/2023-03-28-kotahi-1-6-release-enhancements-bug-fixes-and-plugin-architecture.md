---
published: true
title: "Kotahi 1.6 Release: Enhancements, Bug Fixes, and Plugin Architecture"
date: 2023-03-28
intro: Kotahi, an open-source, web-based platform for managing scholarly
  publications, has released its latest version, Kotahi 1.6. This new version
  brings a number of enhancements and bug fixes, as well as a plugin
  architecture for handling both Wax and generic plugins.
icon: /static/images/uploads/task_manager.png
image: /static/images/uploads/task_manager.png
---
Kotahi, an open-source, web-based platform for managing scholarly publications, has released its latest version, Kotahi 1.6. This new version brings a number of enhancements and bug fixes, as well as a plugin architecture for handling both Wax and generic plugins.

One of the major enhancements in Kotahi 1.6 is the ability to schedule Task Manager notifications. This means that admins can now create a task and select an email notification reminder to be scheduled when a task is added to a Task template. Editors, on the other hand, can add an assignee to an existing task and click 'Start' when they're ready. They can also create new tasks, schedule reminder notifications, or send an action email notification immediately (e.g., reviewer invitation) from the Task & Notifications page.

Clicking on 'Start' applies the 'Duration' count selected in the Task builder and sets the 'Due date'. Email notifications will be sent 'x' days 'before'/'after' a 'Due date' is met. Marking a task as 'Complete' suppresses email notifications from being sent.

In addition to these enhancements, Kotahi 1.6 also includes bug fixes. For instance, MathML displays now work correctly in PDFs generated from the Production editor. Footnotes are also displayed correctly in PDFs generated from the Production editor.

Finally, Kotahi 1.6 introduces a plugin architecture that provides a technical overview for handling both Wax and generic plugins. This markup makes it easier to develop and manage plugins in Kotahi.

Overall, Kotahi 1.6 is a major release that brings significant enhancements and bug fixes to the platform. With its new plugin architecture, it also provides a solid foundation for future development and customization. If you're currently using Kotahi, we highly recommend upgrading to version 1.6 to take advantage of these new features and improvements.

More information - <https://kotahi.community>
