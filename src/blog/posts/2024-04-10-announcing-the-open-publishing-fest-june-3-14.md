---
published: true
title: Announcing The Open Publishing Fest!!! (June 3-14)
date: 2024-04-10
intro: >
  We are pleased to announce the third Open Publishing Fest (June 3-14), a
  decentralized public event that brings together communities supporting open
  source software, open content, and open publishing models. This year's fest
  will be held during two weeks in June, featuring discussions, demos, and
  performances that showcase our paths toward a more open world.
icon: /static/images/uploads/screenshot-from-2024-04-10-14-20-19.png
image: /static/images/uploads/screenshot-from-2024-04-10-14-20-19.png
---
[https://openpublishingfest.org/](https://openpublishingfest.org/)

We are pleased to announce the third [Open Publishing Fest](https://openpublishingfest.org), a decentralized community event that brings together communities supporting open source software, open content, and open publishing models.

This year's fest will be held during two weeks in June, featuring discussions, demos, and performances that showcase our paths toward a more open world.

The Open Publishing Fest celebrates communities developing open creative and
technological publishing projects. It's an opportunity to connect people and showcase the amazing work in publishing occurring around the world and across sectors and industries. The fest is built on collaboration and distributed participation, with sessions hosted by individuals and organizations around the world in various formats such as panel discussions, fireside chats, demonstrations, workshops, and performances.

We invite you to join us by [proposing as many sessions as you like](https://openpublishingfest.org/form.html).

You have the freedom to choose your topic, participants, timezone, time, technology, and language. We welcome an enlightened self-interested approach - talk about and promote the ideas and projects you love but do so in a generous and engaging manner. It's your chance to say what you need to say. Don't be afraid of being too conceptual or too technological - speak to what you think is important.

The Open Publishing Fest started in the first year's gloom of COVID to bring happiness to our community. We are carrying the event on to facilitate some easy and light sharing of the important work you are doing.

To propose an event for inclusion, please use the [proposal form](https://openpublishingfest.org/form.html). Don't forget to use the #OpenPublish tag when tweeting about the fest!

### More info

For more information please email adam@coko.foundation.

We are open to sponsorship, so if you would like to help us find funding or sponsor the event, please also contact Adam at adam@coko.foundation.

Participant Guidelines, Privacy Policy, and Event Expectations can be found on the [OPF website](https://openpublishingfest.org/).

All code for the fest is open source and can be found at [[https://gitlab.coko.foundation/open-publishing/openpublishingfest](https://gitlab.coko.foundation/open-publishing/openpublishingfest)](https://gitlab.coko.foundation/open-publishing/openpublishingfest)

We look forward to your participation in the Open Publishing Fest 2024!
