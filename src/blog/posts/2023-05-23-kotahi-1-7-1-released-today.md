---
title: Kotahi 1.7.1 released today!
date: 2023-05-23
published: true
intro: Its a 'minor' release but lots of good updates for our best in class
  journal and preprint/PRC platform. Multitenancy coming in a few weeks!
icon: /static/images/uploads/Kotahi.png
image: /static/images/uploads/Kotahi.png
---
**Release notes:**

A **’Submit new version’** action has been added to the Configuration Manager. When enabled, authors are able to submit a new version without requiring an editor submitted a decision verdict (accept, revise or reject).

![submit_new_version](https://gitlab.coko.foundation/kotahi/kotahi/uploads/a8b029e9cd794e7028d171ae7f37b3d8/submit_new_version.png)

**ThreadedDiscussion** form field engagement history is now displayed across versions.

![ThreadedDiscussion_version_history](https://gitlab.coko.foundation/kotahi/kotahi/uploads/72346128665bb9e9d237c5c6a359d147/ThreadedDiscussion_version_history.png)

Easily unassign Editor roles from the Control panel>Teams>Assign Editors dropdown menu using the cancel action.

![unassign_editor](https://gitlab.coko.foundation/kotahi/kotahi/uploads/b1c2c2b0eb43b82ab4cbaf738ee170ad/unassign_editor.png)

**Other;**

* Node has been upgraded to version 16. This was done in conjunction with a Cypress Docker upgrade.
* **XSweet** has been added as a microservice.

**Bug fixes;**

* Publish action is now working again on the `aperture` and `colab` instance archetypes.
* Task Manager; error message erroneously indicating that email notifications are not being sent has been fixed.
* A record of unresponsive reviewers are now kept across versions. Access to a review is restricted to Reviewers who have ‘accepted’ an invitation to peer review (version 1, for example), have done so after a new version of the manuscript has been submitted (version 2, for example).
* Numerous updates and fixes to `aperture` Cypress tests.
* Fix for JATS generation, and
* better handling of broken images in Wax.
