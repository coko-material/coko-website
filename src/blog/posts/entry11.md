---
published: true
title: "Peerwith chooses Kotahi"
date: 2022-02-25
intro: Peerwith announcement about their adoption of Kotahi!
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/peerw.png"
---

![Coko Website](/images/uploads/peerw.png)

Ivo Verbeek, Peerwith Director, comments: “We selected Coko’s Kotahi as a modern, innovative platform and the best match with our requirement for building a fully featured publishing platform from submission to publication. We especially like the Single Source Publishing model where multiple actors – authors, editors, and also Peerwith experts – work on a single source, through the Kotahi workflow-first system.”

Full press release [here](http://blog.peerwith.com/peerwith-adopts-cokos-kotahi-the-modern-scholarly-publishing-platform/).
