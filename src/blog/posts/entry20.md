---
published: true
title: "Update from Croatia"
date: 2022-08-31
intro: We have been working hard in Croatia!
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="ssa" src="https://peertube.coko.foundation/videos/embed/baea247d-9ba3-42ed-9bdb-8a494c70b4d1" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/ssa.png"
---
The Coko management met in Croatia for a few days planning. First time in 3 years! Here is a brief (and a little rough) update!
