---
title: Kotahi Multitenancy One Step Closer...
date: 2023-06-20
published: true
intro: Kotahi is about to wake some folks up...
icon: /static/images/uploads/gitlab.png
image: /static/images/uploads/gitlab.png
---
[Kotahi](https://kotahi.community) (our Journal and Preprint platform) is an extremely powerful application. What's the biggest secret about Kotahi? Well, Kotahi itself. We have been quietly working on this for several years and we are getting close to the turn key solution we always wanted including:

* Drag and drop submisison form building
* Sophisticated API integrations
* Internal basic and advanced CMS modes
* Internal production interfaces for producing JATS and PDF at the push of a button
* Workflow configuration
* Video chat, text chat

and a whole lot more...

But coming soon in the forthcoming Kotahi 2.0 release...multitenancy (see pic)...what this space!
