---
published: true
title: "Introducing the Future of PDF Design: The Kotahi AI PDF Designer"
date: 2024-03-04
intro: |
  We're excited to bring you a groundbreaking innovation that's set to 
  transform the way we think about PDF production: the Kotahi AI PDF 
  Designer. This revolutionary tool is not just an advancement; it's a 
  complete reimagining of the PDF design process, tailored to meet the 
  needs of today's publishers, researchers, and content creators.
video: https://vimeo.com/917703280
icon: /static/images/uploads/screenshot-from-2024-03-04-19-01-35.png
---
For more information read Coko Newsletter #46 : [https://mailchi.mp/bc9d240126e6/coko-newsletter-8307188](https://mailchi.mp/bc9d240126e6/coko-newsletter-8307188)
