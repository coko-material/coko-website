---
published: true
title: "Coko Newsletter number 18 out now!"
date: 2022-06-04
intro: Featuring - It's a bumper GEEK issue! We cover some recent Coko technology developments including - Pagedjs Template Sprint (OER), Wax Question Model Widgets, Libero Editor Consortium, PDF ingestion, Kotahi Multitenancy.
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/Coko.png"
---

![Coko Website](/images/uploads/Coko.png)

It's a bumper GEEK issue! We cover some recent Coko technology developments including - Pagedjs Template Sprint (OER), Wax Question Model Widgets, Libero Editor Consortium, PDF ingestion, Kotahi Multitenancy.

Read and/or subscribe here:  [https://mailchi.mp/9e69aeb98c08/coko-newsletter-6816913](https://mailchi.mp/9e69aeb98c08/coko-newsletter-6816913).
