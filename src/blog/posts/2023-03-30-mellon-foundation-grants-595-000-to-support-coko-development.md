---
title: Mellon Foundation Grants $595,000 to Support Coko Development
date: 2023-03-30
published: true
intro: >-
  FOR IMMEDIATE RELEASE


  Mellon Foundation Grants $595,000 to Support Coko Development


  San Francisco, CA - Coko is delighted to announce that the Andrew W. Mellon Foundation has awarded a grant of $595,000 over 2 years to support the continued development of [Ketida](https://ketida.community) (formerly called Editoria), the innovative web-based book production platform.
icon: /static/images/uploads/ketida.png
image: /static/images/uploads/ketida.png
---
FOR IMMEDIATE RELEASE

Mellon Foundation Grants $595,000 to Support Coko Development

San Francisco, CA - Coko is delighted to announce that the Andrew W. Mellon Foundation has awarded a grant of $595,000 over 2 years to support the continued development of [Ketida](https://ketida.community) (formerly called Editoria), the innovative web-based book production platform.

The continued support of the Mellon Foundation is a testament to the strength of Coko's community-based open-source approach and the significant impact of Ketida in the publishing industry.

Adam Hyde, Coko founder, commented, "We are very excited that Mellon continues to support our work for the last 5 years, and it showcases the long term support the community is continuing to invest in Coko's technology and community based open source approach."

Ketida has revolutionized book production by providing a streamlined, single-source, web-based platform that allows publishers to collaborate and produce books in a fraction of the time it takes using traditional methods. The platform's innovative features have made it a favorite among publishers and authors, with its intuitive user interface and unparalleled customization options.

If you are interested in using Ketida for your publishing needs, please contact [adam@coko.foundation](mailto:adam@coko.foundation).

About Coko : Coko is a non-profit technology organization with offices around the world that develops open-source publishing platforms to support the publishing industry. Their mission is to improve the ways that knowledge is shared by creating tools that are open, transparent, and easy to use.

Contact: Adam Hyde Founder, Coko Foundation [adam@coko.foundation](mailto:adam@coko.foundation)

<https://ketida.community>

<https://coko.foundation>
