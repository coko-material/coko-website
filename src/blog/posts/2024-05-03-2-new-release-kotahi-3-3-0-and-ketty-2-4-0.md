---
published: true
title: "2 new releases: Kotahi 3.3.0 and Ketty 2.4.0"
date: 2024-05-03
intro: >
  We're very happy to announce the launch of Kotahi 3.3.0 and Ketty 2.4,
  bringing advanced features to enhance your digital publishing experiences. 
icon: /static/images/uploads/file_browser2.png
image: /static/images/uploads/file_browser2.png
---
Full information here: [https://mailchi.mp/655f2409d7bc/coko-newsletter-8312280](https://mailchi.mp/655f2409d7bc/coko-newsletter-8312280)

Kotahi 3.3.0 introduces a robust CMS with editable file trees, customizable publishing website layouts, collections management, and new metadata management tools, making publishing site organization and creation more intuitive and powerful than ever. 

Ketty 2.4 enhances book production with new sharing options, an admin dashboard, and AI integration capabilities, ensuring a seamless and inclusive user experience. 

Both platforms continue our commitment to open source innovation. Join us in advancing publishing by visiting our websites or contacting Adam Hyde for more information and collaborations. 
