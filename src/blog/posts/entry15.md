---
published: true
title: "Push Button JATS Production now here!"
date: 2022-05-14
intro: We have been working hard on an exciting new innovation.
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Push Button JATS Production with Kotahi (long ramble)" src="https://peertube.coko.foundation/videos/embed/79398714-ad82-46f4-863b-357d2c763585" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/kotahi-JATS-vid.png"
---
See also [https://coko.foundation/articles/kotahi-a-new-approach-to-jats-production.html](https://coko.foundation/articles/kotahi-a-new-approach-to-jats-production.html)
