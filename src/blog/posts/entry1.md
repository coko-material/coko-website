---
published: true
title: "Rant #1: Word Processors" 
date: 2021-12-10
intro: The start of a new series of longer from rants on video. This is about Word Processors and why they might be a lot more important than you think!
author: Adam Hyde
tags: featured
class: 
image: ""
video: "/images/uploads/Word-Processor-Rant-2.mp4"
icon: "/images/uploads/rant-1-screenshot.png"
---

