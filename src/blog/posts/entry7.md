---
published: true
title: "Coko website almost done!"
date: 2022-02-16
intro: We have been updating the Coko website and it is almost finished!
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/Coko.png"
---

![Coko Website](/images/uploads/Coko.png)

We have been updating the Coko website for the last few months in a very iterative manner. THings have moved around a little over this period as we refine the site. If you have any feedback OR if you are looking for anything from our old site and can't find it then please reach out to us with your feedback and requests!
