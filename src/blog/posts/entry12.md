---
published: true
title: "Publishers vs The Web"
date: 2022-03-04
intro: Some whimsical theory about the web and publishing that hopefully makes sense.
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Publishers vs the web" src="https://peertube.coko.foundation/videos/embed/0b2517ec-6530-4dbc-a926-7d59c1a789a6" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/webvsp.png"
---
