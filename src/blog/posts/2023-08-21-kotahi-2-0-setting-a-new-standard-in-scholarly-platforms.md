---
published: true
title: "Kotahi 2.0: Setting a New Standard in Scholarly Platforms"
date: 2023-08-22
intro: >
  _Today represents the release of a key milestone for the project - Kotahi 2.0._
icon: /static/images/uploads/kotahi-logo.png
image: /static/images/uploads/kotahi-logo.png
---
For years, academic publishing has relied on legacy systems that hinder efficiency and scholarly communication. Complex interfaces frustrate users. Rigid, one-size-fits-all submission systems cannot accommodate diverse content, workflows, or journal requirements. Email-driven communications cause delays. Production remains siloed externally without automation. Monolithic architectures hamper scalability.

At Coko, we believe it's time to move forward. Driven by a passion for improving research sharing, we developed Kotahi to tackle these systemic challenges. Initially funded through our surplus and later with help by partners like eLife who share our vision, Kotahi is designed to modernize workflows for traditional and emerging publishing models.

Today represents the release of a key milestone for the project - [Kotahi 2.0](https://kotahi.community/releases/).

The platform tackles key challenges - making standards like JATS attainable without technical staff. It provides enormous flexibility to customize workflows as needed. Sophisticated multitenancy enables the hosting of multiple journals, preprint review, and preprint servers within a single install. Automated PDF production enables consistent, high-quality typesetting at scale. The microservices architecture allows independent scaling and updating of components. 

Scholarly publishing is evolving rapidly, and Kotahi is designed to improve how research is currently shared while anticipating future needs. By improving existing workflows and anticipating emerging models, Kotahi represents the innovative future of research communication.

The following is a brief summary of some key Kotahi 2.0 features.

## Multitenancy Supporting Diverse Use Cases

Managing multiple scholarly publishing groups like journals, preprint servers, or preprint review communities has traditionally required running separate softwares for each one. This quickly becomes cumbersome and difficult to scale.

Kotahi resolves this issue by providing native multitenancy support for many use cases within a single integrated system. This allows a single Kotahi installation to host any number of journals, preprint servers, or preprint review communities in the one installation.

Each group functions independently with its own:

- Isolated data and privacy
- Customizable submission workflows
- Unique look and feel
- Publish endpoint

The key benefits of Kotahi's multitenant approach include:

- Reduced software overhead since only a single instance needs to be maintained
- Workflows tailored for each group without conflicts
- Easy scalability when adding new groups
- Streamlined cross-group analytics and reporting
- The ability to experiment with new publishing processes within the one system

This architecture substantially reduces the complexity and costs of operating diverse portfolios of journals, preprint servers, and review communities. As publishers scale up their groups, new ones can be easily onboarded while retaining custom workflows.

By supporting this kind of sophisticated multitenancy natively, Kotahi establishes a new standard for flexibly managing any number of publishing groups, with any number of different workflows and use cases, on a single streamlined platform.

## A Modern, Powerful, CMS

Kotahi incorporates a modern content management system (CMS) based on static site generation rather than a traditional database-request-driven CMS. This approach provides significant advantages in speed, security, and scalability.

Compared to a traditional CMS which can become sluggish and vulnerable at scale, Kotahi's CMS remains lightning fast and rock solid regardless of traffic or content volume. This innovative architecture ensures publishers can manage content efficiently while providing users with a reliably fast experience. Full text articles are published at a push of a button. The CMS can handle the demands of complex scholarly publishing requirements both now and in the future.

## Automated JATS Production

The Journal Article Tag Suite (JATS) XML standard has become ubiquitous in scholarly publishing, allowing content to be structured, exchanged, and preserved in a consistent machine-readable format. However, many publishers find the JATS format complex, costly and cumbersome to implement. The specialized skills and effort required drives up costs, hampers efficiency, and creates barriers to adopting this critical publishing standard.

Kotahi aims to change this status quo by integrating JATS production seamlessly into the publishing workflow. 

At its core, Kotahi sees a document as a constellation of content, primarily in the form of the manuscript text itself, and associated metadata. Both categories of information are transparent to Kotahi as the system ingests manuscripts at submission time and converts them to an internal file format. Customizable submission forms designed in Kotahi gather comprehensive metadata upfront.

The internal production editor then provides an intuitive way to ready content for JATS export without needing direct XML skills. Users simply visually highlight and tag content sections, with the selections automatically mapped to the appropriate JATS document elements. On demand, Kotahi converts the prepared manuscript and metadata into validation-checked JATS XML that complies with all specifications. This standards-compliant JATS file can also be regenerated as needed throughout the editorial workflow. Kotahi also has an evolving set of tools for the management and validation of citations.

By integrating JATS production into the publishing pipeline in this tailored yet automated manner, Kotahi makes adopting JATS accessible to mainstream publishers. The platforms innovative production tools lower the barrier to entry while facilitating standards compliance at scale, reducing overhead costs and accelerating publishing turnarounds.

## Automated Production of PDF

One of Kotahi's standout features is the ability to automatically typeset and generate print-ready PDFs with just a click. This is powered by the integration of [Pagedjs](https://pagedjs.org) (also built by Coko), an open-source library for paginating HTML content into high-quality PDF documents.

As manuscripts in Kotahi are edited and stored as HTML, [Pagedjs](https://pagedjs.org) can fragment the content into pages, inject sophisticated print styling, and paginate a preview right within the browser. When ready, the print-perfect PDF can be saved out (or batch processed) - no manual typesetting required.

This browser-based PDF generation approach also enables automated workflows. Kotahi can thus produce press-ready PDFs at scale in a fraction of the time and at little or no cost. The result is a system that can take manuscripts from writing to publication-quality typesetting with unparalleled efficiency. Hands-off, consistent, and aesthetically pleasing PDFs become accessible to publishers of all sizes through Kotahi's simple automation.

## Drag and Drop Submission Form Creation

Another of Kotahi's standout features is the ability to create submission forms using an intuitive drag and drop interface. This removes the need for any coding knowledge or technical expertise. Users can easily add, remove or rearrange form fields as needed.

Whether working from pre-made templates or designing fully custom forms, the entire process is quick and user-friendly. For authors, filling out submissions becomes simpler, with reduced chances of errors. This results in more complete information being captured upfront, benefitting downstream teams.

Compared to traditional form creation methods which are often complex and time-consuming, Kotahi's drag and drop approach simplifies submission management for publishers and authors alike.

## Support for Any Metadata Schema

The submission process for journals and other preprints requires capturing metadata - information about the manuscript itself. However, traditional systems often force standardized schemas that lack flexibility.

Kotahi provides versatility by supporting submission and publication using any metadata schema, including custom schemas or niche standards. This adaptability lets publishers capture optimal metadata for their domain and needs.

Kotahi enables through a number of methods, but largely through its drag-and-drop form builder where any metadata tags can be attached to submission fields. It also supports complex nested data via custom form elements, for capturing detailed author information for example.

For authors, submitting manuscripts is simpler when providing metadata in appropriate domain-specific schemas. For publishers, post-submission overheads are reduced by capturing comprehensive, flexible metadata upfront.

This metadata can then be configured to be pushed through to the appropriate publishing end points.

By empowering customized metadata capture, Kotahi ensures journals and publishers get the specific information they need while authors provide it painlessly. This metadata versatility represents a key advantage of Kotahi's submission system.

## Tailored Peer Review Process

The peer review process is a crucial element of scholarly publishing. However, traditional systems often enforce a rigid, one-size-fits-all approach.

In contrast, Kotahi provides flexibility to tailor the review process to each journal or review community's specific needs. Different models like open, blind, or double-blind reviews can be configured. Reviewers can also collaborate on shared reviews while also providing individual feedback if desired.

Kotahi enables customizing the level of author participation as well. Review workflows can allow authors to respond to reviewer comments via threaded discussions if needed. Annotations (comments) directly on manuscripts is also supported.

Kotahi aims to facilitate constructive conversations between authors and reviewers to improve manuscripts. As peer review evolves towards more collaborative exchange, Kotahi provides built-in tools ready to enable
this emerging review model.

By supporting tailored review workflows Kotahi ensures you can design the peer review model best suited for your needs. This flexibility and customizability result in higher quality, more meaningful reviews.

## Real-Time Communication Tools

Effective communication between authors, editors, and reviewers during the submission and peer review process is key. However, email is not always the ideal medium for these interactions.

Kotahi instead incorporates real-time communication features including live chat and video chat to facilitate conversations as needed. All communications become visible within the platform, eliminating email clutter.

Users can quickly get answers, resolve issues, and streamline collaboration all within Kotahi's interface. Smooth end-to-end discussions from submissions to publication become possible.

## Customizable Task Management

To streamline worklow, Kotahi incorporates advanced but customizable task management capabilities. Project boards provide an overview of tasks and workflows across the publishing lifecycle.

Granular controls allow configuring task management at a per-journal or even per-manuscript level. Automated reminders, actions and invitations are configurable within the task manager itself.

This results in workflows that are tailored to each manuscript's unique needs.

## Versioning

Kotahi supports versioning of submissions to track revision history across review and editing cycles.

Each new submission round creates a new version of both the manuscript content and submission metadata. This allows tracking a submission from initial draft to final published form.

Versioning provides a record of all changes while ensuring users only interact with the current definitive version.

This version control system is crucial for collaborative workflows where manuscripts go through many stakeholder hands. Kotahi maintains submission integrity and clarity through automated version tracking.

## AI-Powered Preprint Recommendations

Kotahi also has some advanced features to support preprint review. For example, Kotahi enables AI-powered preprint recommendations that assist curators in identifying relevant manuscripts for review.

For preprint servers like bioRxiv new submissions in specified subjects are automatically imported. Curators can then select preprints of interest for review.

Based on these selections, Kotahi leverages various APIs and AI services to recommend related preprints that may also warrant review. Checks help ensure only recent submissions from a preconfigured time period, and from approved preprint servers, are suggested.

This tailored AI-matching allows curators to rapidly pinpoint the most pertinent preprints to evaluate and disseminate within their field or community. It enhances the efficiency of preprint screening while surfacing hidden gems curators may have otherwise missed.

By combining Kotahi's infrastructure with the power of AI powered recommendations, the platform aims to accelerate preprint discovery and review - getting impactful research into the right hands faster.

## Configurable, Customizable, and Extensible

Kotahi is designed to be highly configurable, customizable, and extensible to meet diverse publishing needs.

The system already allows individual configuration of workflows, review models, metadata schemas, CMS and publish endpoints, task management, and more per group. This enables tailored setups without software the need for developer assistance. 

Further customisation can be achieved through integrations, plugins, addition of new microservices or new functionality and the Kotahi team is ready to support organizations wanting to customize, extend, or configure the system to their needs. Whether it's tailored workflows or custom enhancements, Kotahi provides the versatility to meet publishing requirements and Coko is here to support you.

## Microservice Architecture

Kotahi is built using a modular microservice architecture. In contrast to monolithic platforms, components are designed as independent services that work together.

This provides benefits like:

- Scalability: Services can be individually scaled as per demand.
- Flexibility: Services can be modified or replaced without affecting others.
- Resiliency: Issues with one service don't propagate across the system.

As publishing needs grow and evolve, Kotahi's microservices make scaling, upgrading, and maintenance simpler and more cost-effective.

## Smooth Installation and Deployment

Although designed for flexibility, Kotahi offers straightforward installation and deployment. It employs a Docker-based architecture with modular microservices that reduce complexity.

While basic sysadmin skills are recommended, Kotahi aims to streamline the initial setup process. Once deployed, the system can scale groups and content without requiring deep technical knowledge.

Ongoing enhancements continue to simplify installation and configuration further. The goal is an accessible system where publishers can easily leverage Kotahi's capabilities with minimal engineering overhead.

With its lightweight microservices and consolidated design, Kotahi enables publishing teams to get started swiftly and focus on content creation rather than technical hurdles.

## 100% Open Source

Kotahi is published under open source licenses, with all source code freely available. This enables publishers full freedom to use, modify, and distribute the software to meet their needs.

As open source software, Kotahi benefits from community contributions and transparency. Anyone can inspect the codebase, propose improvements, report issues, or create customizations.

This open development model also helps drive rapid innovation as the platform evolves via a kind of ‘public peer review’. Organizations can collaborate to extend Kotahi rather than reinventing the wheel.

By being 100% open source, Kotahi represents a publishing platform unencumbered by proprietary restrictions. This liberates publishers to fully utilize Kotahi as a strategic asset customized for their requirements.

## Conclusion

In summary, Kotahi offers a refreshing modern take on scholarly submission management. The platform aims to solve multiple pain points faced by publishers and researchers when using legacy solutions or when contemplating new ways of working.

Features like intuitive drag and drop interfaces, flexible metadata support, typeset automation, multiple use-case multitenancy support, real-time communication, and modular architecture set Kotahi apart. The system streamlines and connects workflows end-to-end across the research lifecycle.

As publishing demands continue to evolve, Kotahi represents the next-generation of scholarly platform. Its innovative approach promises to enhance everything from author experience to improved forms of publishing, setting a new standard for the industry..

For more information contact [adam@coko.foundation](mailto:adam@coko.foundation)
https://kotahi.community/releases/
