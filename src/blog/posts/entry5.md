---
published: true
title: "Pandoc Integrates Pagedjs"
date: 2022-02-10
intro: Popular conversion tool Pandoc integrates Pagedjs"
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/pandoc3.png"
---

![pandoc](/images/uploads/pandoc.png)

Pagedjs is Coko's open source, standards based typesetting engine. Pagedjs is a free and open source JavaScript library that paginates content to create PDF output from any HTML content. This means you can design works for print (eg. books) using HTML and CSS!

The good news is that the good folks over at the Pandoc community have included our very Pagedjs natively into their tool! The 2.17.1 release of Pandoc has Pagedjs inside and that means plenty of folks can start using our typesetting engine to make beautiful PDFs.
Much love and gratitude to Albert Krewinkel who did all the hardwork integrating the two tools. Amazing!

This could very well be a game changer as the 2.17.1 release of Pandoc really broadens the reach of Pagedjs dramatically. If you are one of these new users then head on over to the following channels to learn more about how to use Pagedjs!
Coko Chat - https://mattermost.coko.foundation
Pagedjs site: https://pagedjs.org
