---
title: FLAX Arrives
date: 2023-05-22
published: true
intro: Our new publishing front end and web presence platform for content,
  arrives. The first project to host content in FLAX is... the Louvre!
icon: /static/images/uploads/flax.png
image: /static/images/uploads/flax.png
---

Coko has played a key role in producing a new and stunning [multiformat book on the artwork of Antoon Van Dyck](https://livres.louvre.fr/vandyck/) published by the Musée du Louvre. The catalogue, meticulously crafted, showcases the collections of Van Dyck's works at the museum. It was produced with Coko's groundbreaking software tools, FLAX and Paged.js, marking a significant milestone in the museum and scholarly publishing sector.

![FLAX Site](/images/uploads/fwuz-xswwaeuat8.jpeg "FLAX Site")

Adam Hyde, Coko Founder, said, "We are thrilled that the Musée du Louvre chose to use our FLAX and Paged.js tools for the production of this amazing catalogue. We are proud to show that these tools are truly the best of breed in the industry."\
\
The catalogue, titled "Antoon Van Dyck: Catalogue raisonné des tableaux du musée du Louvre," provides a detailed study of the masterpieces of Van Dyck visible at the Louvre.\
\
FLAX (website forthcoming), a publishing front end and web presence for content, has been a significant focus for Coko, designed to help publishers, including those in the scholarly and museum sectors, host beautiful digital books and journals online. Similarly, [Paged.js](https://pagedjs.org) is a free and open-source JavaScript library that paginates content in the browser to create PDF output from any HTML content.\
\
"We have been working on FLAX for a long time, and it's rewarding to see it used in such a prestigious context," said Hyde. "The fact that FLAX and Paged.js were used by such a respected institution as the Musée du Louvre is a validation of our work and the open source community as a whole."\
\
The project was also a collaboration of many folks including some staff and friends of Coko -  Julien Taquet, who worked on the design and development of the site and the publishing tools; Agathe Baëz, responsible for the graphic design, integration, and layout; and Nicolas Taffin from C&F Editions, project manager, and publisher.\
\
"We're incredibly proud of our colleagues and friends, Julien, Agathe, and Nicolas and the amazing work they, and the team at the Louvre, have produced." Hyde added.\
\
The online catalogue hosted by FLAX can be accessed here: <https://livres.louvre.fr/vandyck/>\
The PDF of catalogue produced by Pagedjs can be found here: <https://livres.louvre.fr/vandyck/Ducos-Blaise_Antoon-Van-Dyck-Catalogue-raisonne_Louvre-editions_2023.pdf>
