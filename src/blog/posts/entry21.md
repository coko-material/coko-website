---
published: true
title: "Understanding Workflow-first Design"
date: 2022-09-22
intro: First in a new two part series of articles on Workflow-first design principles by Coko Founder Adam Hyde..
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/Adam-C-V1b.jpg"
---

## Part 1: ~~Technology!~~  Workflow!

In this 2-part series, I’ll be looking at a critical and under-examined step required to build publishing technology - optimizing workflows.

I am in the business of building publishing technology. However, I actually believe the core of what I do is designing and optimizing publishing workflows. Technology is just an (optional) outcome of that process. 

To get to good technology, it is very important to start the process by not thinking about technology at all. If you frame your thinking through the lens of technology, you will more than likely end up with an expensive failure. What we are really interested in are good workflows and, later, designing technology to encapsulate and enable good workflow.

![Coko Website](/images/uploads/Adam-A-V1.jpg)

To help us understand this problem we need to define workflow as it means many different things to different people - by workflow, I mean the complete set and order of actions an organization (usually a publisher) undertakes to prepare an object (book, journal article, preprint review, question set etc) for publishing or sharing. 

For example, workflow is everything that is done from start to finish that falls within a publisher's control, to prepare a book or journal article for publication. Or, every action required by a community to prepare and share reviews on preprints. 

*Note: I use the term ‘publisher’ very liberally (see definitions at bottom).*

What publishers want is to improve their publishing processes, but they tend to think purely in terms of technology as both the problem and the solution.  As a result of this ‘technology first’ thinking the process for many publishers looks like this:

1. Choose a new technology
2. Use the technology

It is worth noting that there are some good motivations for wanting to change technology which do not originate in trying to improve workflow. Liberating an organization from expensive licensing of proprietary technology, for example, is an increasingly common driver.  

Replacing unstable legacy technology is also a good motivation.

However, regardless of the motivation, if you are determined to change technology, you should also see this as an opportunity to improve how you work - to improve your workflow. 

As mentioned above, the “technology first” approach can be problematic. Three common outcomes of technology-first thinking are:

1. You can make an expensive technology change without improving the publishing process. 
2. You may create new problems and poorer outcomes for the publishing process.
3. A ‘rush to adopt’ can come with unforeseen tradeoffs that later may prove very costly.

These undesirable outcomes are better avoided by adopting a workflow-first framework.

For new entrants and incumbents alike, a workflow-first process looks like this:
1. Understand the current (or speculative) workflow
2. Optimize the workflow
3. Design technology to enable the workflow
4. Build (or choose) the technology (optional)

In these two articles we will look at steps 1 and 2 only. Steps 3 and 4 might require a thesis.

**Step 1: Understand the current workflow** - is necessary to generate an understanding of what you are actually doing right now. This step needs to drill down into the nitty gritty of the daily workflow so you can get a complete picture of what actually happens ‘on the floor’. High level executive summaries are not what we are after. We need to know what actually happens.

Without understanding exactly what you are doing now, improving what you do will be based on false assumptions. As poet Suzy Kassem has said: 

>"Assumptions are quick exits for lazy minds that like to graze out in the fields without bother."

When examining workflow we need to bother people. To ask hard questions and get to the bottom of things. I’ve often heard folks say they know their workflow, or someone within their organization knows the workflow. But after asking a lot of exploratory questions I inevitably find that no one person understands exactly what happens at all stages of the workflow. Consequently, understanding your current workflow, to the granularity required to move to the next steps, requires a process in itself. But we will discuss this in more detail in the next article.

However Step 1, while important (and important to get right), is just prep for the most critical step of the entire process: 

**Step 2: Optimize the workflow** Optimizing the workflow is what every publishing technology project should aim to do. If you don’t optimize the workflow, if you haven’t improved what you do, then what have you achieved? It would make sense then, to prioritize thinking about improving workflow before you start to think about technology.

![Coko Website](/images/uploads/Adam-B-V1.jpg)

Optimizing the workflow is, in itself, a design problem and requires dedicated effort. Unfortunately this step is often skipped or paid only a passing tribute. Conflating steps 2 and 3, for example, is typical of the popular publishing RFP (Request for Proposals) process (see https://upstream.force11.org/can-we-do-away-with-rfps/) :

1. Understand the current (or speculative) workflow
2. Design technology to enable the workflow
3. Build (or choose) a new technology (optional)

An RFP process may be motivated by a few big ticket items that may need improving (or licensing issues as per above), but often lacks a comprehensive design process aimed at optimizing the workflow. Instead, technology is chosen because it fulfills the requirements of the current workflow audit (Step 1) with a few possible fixes - this is because most RFP processes focus on updating technology rather than improving publishing processes. Consequently there are minimal, if any, workflow gains and a failed opportunity to identify many optimisations that can deeply affect a good technology choice and improve how you publish. 

To avoid these problems and to improve how you publish, it is really necessary to focus dedicated design effort to optimizing workflow. 

The kind of process I’m talking about was once brilliantly described to me by a friend Martin Thompson (Australian Network for Art and Technology) a long time ago. Martin was explaining to me the principles of video compression for streaming media, but the principles can be applied to workflow optimisation. Imagine a plastic bag full of air with an object in it (let's say a book). The air represents all the time and effort required to produce that book. Now suck out the air, and we see the bag and book remain the same but the volume of air has reduced. We have reduced the effort and time to produce the book, while the book remains unchanged. 

![Coko Website](/images/uploads/Adam-C-V1b.jpg)

This is a simple but clear metaphor for what we are trying to achieve. In summary, optimizing workflow is about saving time and effort while not compromising the end product. 

Workflow optimization requires an examination of every stage of the process (as discovered in step one of our workflow-first process) and asking if that stage can be improved or eliminated. We are also asking some other questions such as -
- Can anything be improved to help later stages be more efficient?
- Are there  ‘eddies’ in a workflow that can be fixed? 
- Can operations occur concurrently rather than sequentially?

When asking these questions we will discover many efficiencies and we can slowly start drawing out a complete improved workflow. Some steps will disappear, some will be improved by mildly altering the order of operations, others will be improved by small tweaks. Some changes are more significant and may require greater change.

Once we have been through every stage looking for efficiencies, once we have our complete and optimized workflow, we are ready to start thinking about what we need to enable these changes. Technology may be part of the answer, but it is never the only answer, and in some cases it is not the answer at all. If we do decide new technology is required then only now should we start on the path to designing or choosing the tools to support our optimized workflow. 

For the next article we will look at what a methodology for understanding and optimizing workflows can look like - Workflow Sprints.

### Some definitions
In preparation for the next part in this series, here are some definitions. As mentioned above, by workflow I mean the complete set and order of actions an organization (usually a publisher) undertakes to prepare an object (book, journal article, preprint review, question set etc) for publishing or sharing. 

For example, workflow is everything that is done from start to finish that falls within a publisher's control, to prepare a book or journal article for publication. Or, every action required by a community to prepare and share reviews on preprints. 

‘Actions’ that occur in this process are events performed by both :
humans (e.g. reviewing, formatting, Quality Assurance, copy editing etc)
computers (e.g. semantic analysis, format conversion etc). 

I use the term ‘object’ to refer to a generic publishable entity e.g. a book, or journal article, dataset, test question or questions, or preprint review.

Lastly, I refer to any organization that prepares information to publish or share as a ‘publisher’. I think many of the folks I work with do not see themselves as (capital P) Publishers. They may instead (for example) identify as self-organized communities that share information (like the preprint communities mentioned above). However, for simplicity's sake, I refer to the wide spectrum of various organizational models that prepare objects to publish or share as (small p) publishers.

For more information about Coko please email Coko Founder Adam Hyde - adam@coko.foundation 
