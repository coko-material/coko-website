---
published: true
title: "Coko Annual Report!"
date: 2022-06-04
intro: The 2021 - 2022 Coko Annual Report is available for download.
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/ar21.png"
---

![Coko Website](/images/uploads/ar21.png)

[Download the 2021 - 2022 Coko Annual Report here!](http://download.coko.foundation/3cct9H8)

We are happy to annouce the first ever Annual Report for Coko is now available! The report is in PDF format, looks lovely, and has a lot of information about what we have been doign over the last year or so.

We hope you find the Annual Report interesting and useful! Feel free to share!

[Download the 2021 - 2022 Coko Annual Report here!](http://download.coko.foundation/3cct9H8)

For more information about Coko please email Coko Founder Adam Hyde - adam@coko.foundation 
