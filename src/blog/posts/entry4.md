---
title: "All About Coko Architects"
date: 2022-02-08
published: true
intro: An introduction to Coko Architects
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="What are Coko Architects" src="https://peertube.coko.foundation/videos/embed/c6f4de20-c88e-4995-abd5-ea5a971ed0ae" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/architects.png"
---
