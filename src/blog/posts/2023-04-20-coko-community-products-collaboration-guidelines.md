---
title: Coko Community Products Collaboration Guidelines
date: 2023-04-20
published: true
intro: Guidelines for contributing to Coko products.
icon: /static/images/uploads/adaminski_a_close_up_of_people_building_a_robot_style_8bit_ae3d56c1-776c-47d4-8d26-362f384964d0.png
image: /static/images/uploads/adaminski_a_close_up_of_people_building_a_robot_style_8bit_ae3d56c1-776c-47d4-8d26-362f384964d0.png
---
Collaboration is crucial for the success of community products such as Ketida and Kotahi. To ensure that all parties involved in the collaboration process are on the same page, a process has been outlined. This process helps to clarify what is trying to be achieved, understand if there are duplicate or complementary features that are already on the roadmap, and solve problems with existing features. In this article, we will outline the process for collaborating on Ketida and Kotahi.

Steps for collaborating on Ketida and Kotahi:

1. **The Proposer approaches the respective Project Manager to discuss the feature:**\
   The Proposer is the person who is proposing a feature for development. The Proposer should approach the respective Project Manager to discuss the feature. This is to help clarify for both parties what is trying to be achieved.
2. **The Proposer writes up the feature as an issue in the project’s GitLab repo:**\
   Once the feature is discussed, the proposer should write it up as an issue in the project's GitLab repo using the feature proposal template. This outline does not need to be comprehensive, but there should be a description of what the feature is, how it affects workflows and interfaces, and some notes about the technical approach. The proposer should include mocks where appropriate.
3. **The Project Manager discusses the proposal with the Lead Developer for the project:**\
   After the proposal is submitted, the Project Manager should discuss it with the Lead Dev for the project. There may be further consultation with other members of the Coko team, if necessary.
4. **The Project Manager and Lead Developer make comments on the proposal:**\
   Once the proposal is discussed, the Project Manager and Lead Developer should make comments on the proposal. These comments should include suggestions for changes (revisions), if necessary.
5. **Revisions by the Proposer occur if necessary:**\
   If revisions are necessary, the Proposer should make the revisions.
6. **The Project Manager and Lead Developer give a thumbs up:**\
   When the proposal is ready, the Project Manager and Lead Dev should give a thumbs up.
7. **Development begins in a feature branch in the project's repo:**\
   Once the proposal is approved, development can begin in a feature branch in the project's repo. The Proposer should push local work to this branch regularly to avoid lost work.
8. **A merge request is made when ready:**\
   When development is complete, a merge request should be made.
9. **The Coko project team check the code and functionality:**\
   The Lead Developer, QA Engineer and Project Manager should check the code and functionality.
10. **Report bugs and make comments for revisions:**\
    If necessary, the Coko project team should report bugs and make comments for revisions.
11. **Revisions occur:**\
    If revisions are necessary, they should be made by the Proposer.
12. **The code is merged by the Lead Developer when ready:**\
    Finally, the code can be merged when ready.

To summarise, successful collaborations require good faith, transparency, and communication. The collaboration guidelines for Ketida and Kotahi have been created to facilitate this by ensuring that all parties involved have a clear understanding of the proposal, development, and merging processes. Importantly, following these guidelines should not cause any delay in development. In fact, if Proposers are ahead of the development team, it can actually accelerate the feature development and delivery process, as there will be less need for large-scale refactors, opportunity for cross-organisational collaboration of specific features, and a reduced likelihood of duplicate efforts.

## Ketida Community

*Resources:*

* Website: [ketida.community](https://ketida.community/)

*Project Manager: Christina Tromp*

* Email: christina@coko.foundation
* [Mattermost](https://mattermost.coko.foundation): @christina

*Lead Developer: Alex Georgantas*

* Email: alex@coko.foundation
* [Mattermost](https://mattermost.coko.foundation): @alexgeo

[](<>)

## Kotahi Community

*Resources:*

* Website: [kotahi.community](https://kotahi.community/)

*Project Manager: Ryan Dix-Peek*

* Email: ryan@coko.foundation
* [Mattermost](https://mattermost.coko.foundation): @ryandix

*Lead Developer: Ben Whitmore*

* Email: ben@coko.foundation
* [Mattermost](https://mattermost.coko.foundation): @benwh
