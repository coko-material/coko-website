---
published: true
title: "New Colab Site"
date: 2022-02-20
intro: Our Partners Biophysics Colab launch their Website!
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/colab.png"
---

![Coko Website](/images/uploads/colab.png)

Our awesome partners over at Biophysics Colab have a [new website](https://www.sciencecolab.org/) up. Biophysics Colab are part of the [Kotahi Community](https://kotahi.community) and utilise a Publish-Review-Curate (PRC) model:

"Biophysics Colab is a community of biophysicists who support the mission and vision of Science Colab. As a fledgling operation, Biophysics Colab developed its service by reviewing membrane protein biophysics preprints, and is now expanding into other biophysical areas. "

We are happy to have Biophysics Colab as part of the [Kotahi Community](https://kotahi.community)!.
