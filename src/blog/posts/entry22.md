---
published: true
title: "Coko Newsletter #22"
date: 2022-09-23
intro: Kotahi Update, PageBreak and more...
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/driveway.jpg"
---
![Coko Website](/images/uploads/driveway.jpg)

New Coko Newsletter out! [Read it here.](https://mailchi.mp/82a1819b5ad9/coko-newsletter-7343975)

Features:
- Kotahi Update (AI, XML Production, Task Manager, Multitenancy)
- PageBreak Conference (San Francisco Oct 27,28)
- New Article Series
- On the road again!

For more information about Coko please email Coko Founder Adam Hyde - adam@coko.foundation 
