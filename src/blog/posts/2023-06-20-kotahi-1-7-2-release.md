---
title: Kotahi 1.7.2 Release
date: 2023-06-20
published: true
intro: Mainly a bug fix release with a few new features.
icon: /static/images/uploads/Kotahi.png
image: /static/images/uploads/Kotahi.png
---
Included in this release:

* The ability to enable individual review submissions as ‘Shared’ has been fixed. When enabled, reviewers can access and read other submitted reviews that have been marked as ‘Shared’.
* Chat text editor stays in focus after a user clicks enter or on ‘send’.
* Fix for Cypress test to run on Firefox.
* Update to `wmf` regex to handle `emf` files on conversion. Unsupported image files are replaced with a ‘broken image’ icon for ease of reference.
* Fix to correctly display task lists across versions.
* Fix for the inclusion of the `Funding source` element on export to JATS.

More [information on all releases here](https://kotahi.community/releases/).
