---
published: true
title: "Kotahi: A new way to produce JATS"
date: 2022-05-07
intro: Coko has a new detailed article on push button JATS creation from Kotahi.
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/wax-JATS.png"
---

![Coko Website](/images/uploads/wax-JATS.png)

Coko folks Dan Visel, Ben Whitmore and Adam Hyde wrote an article for Dan's presentation at JATS-Con22 (https://jats.nlm.nih.gov/jats-con/2022/schedule2022.html).

The article goes into some depth about the Kotahi approach to producing JATS with Wax-JATS. This article documents the actual production implementation of Wax-JATS built into Kotahi (https://kotahi.community). The system requires *no prior understanding* of JATS or XML by a user to produce valid JATS for journals. Together with the PagedJS (https://pagedjs.community) typesetting engine Coko built we can output PDF, JATS and HTML 'automagically' all from the same (single) source. 

The virtue of Kotahi’s approach to JATS production – and perhaps why it is valuable to the wider landscape of JATS – is that it makes the threshold to generate JATS very low, and might bring JATS to a wider base of users, and the process is fast, scalable and cost effective.

Full article [here](https://coko.foundation/articles/kotahi-a-new-approach-to-jats-production.html).
