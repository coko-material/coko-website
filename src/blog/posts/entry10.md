---
published: true
title: "Kotahi and Workflow Optimization"
date: 2022-02-24
intro: Adam discusses how Kotahi accelerates scholarly publishing.
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Kotahi and Workflow Optimisation" src="https://peertube.coko.foundation/videos/embed/9d8efe53-6432-463f-9dbe-52694d8a9abc" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/kotahiopt.png"
---
