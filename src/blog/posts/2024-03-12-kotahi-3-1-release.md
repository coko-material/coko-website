---
published: true
title: Kotahi 3.1 Release!
date: 2024-03-13
intro: >
  Semantic Scholar controls added, author proofing workflow improvements,
  mathtype conversion for ingested MS Word files and more!
icon: /static/images/uploads/Kotahi.png
---
**Semantic Scholar integration**; Group Managers can now select servers to import preprints/journals from Configuration>'Semantic Scholar' section. A checkbox setting to enable/disable the import of preprints from Semantic Scholar has also been added.

![](/kotahi/kotahi/uploads/9f8b3f2420c02f2698a858d2d7d82364/Semantic_Scholar_config.png)

**Author proofing feedback**; has also been refactored. Authors and editors will now access feedback from the Production editor>Feedback page. Permissions have been added to ensure the feedback form is locked when authors are providing feedback, and accessible in a read-only state after a feedback form has been submitted.

New links have been provided to access the Production editor;

- Authors can access the Production editor from the Dashboard>My Submissions>Actions link.

![](/kotahi/kotahi/uploads/6a5720dbb6b279be53abed45f7a774f7/author_dashboard_production.png)

- Editors can access the Production editor from the Dashboard>Manuscripts I'm editor of>>Actions link.

![](/kotahi/kotahi/uploads/b5cf131bcd62bc741a133aa7a207d15f/editor_dashboard_production.png)

There is also a record kept of all submitted feedback per version. Filter by version to see author feedback per round within the context of a manuscript version.

![](/kotahi/kotahi/uploads/4080918cc58e1a2cea314772e0827ad9/feedback_history.png)

**Other enhancements;**

- Flax was updated to improve  `robots.txt`. Previously, only `index.html` was disallowed. If a draft article page was linked from an external public website, a crawler could follow that link and crawl the entire draft site. In non-draft node, a crawler was disallowed from crawling the listing of articles, which would prevent it from indexing the site.
- Update to import DocMaps from the eLife API.
- XSweet flag that turns MathType inserted into Docx into equations accessible in Wax. Please note this is a Beta release. The current solution works most consistently when using output from MathType version 6. We're working on support for MathType version 7.

**Bug fixes;**

- Fix that locked reviewers out of a review on multi-version manuscripts.
- Fix Dockerfile production.
- Fixes to editor assignment, permissions and form migration.
- Moved plugins folder to `packages/server/config/plugins`.
- Fix to hide author proofing email notification templates from the Control panel>Tasks&Notifications page.

**Next release;**

- Various page and menu behaviour and styling changes to improve usability e.g. sticky menus and creating a tabbed view. In the Configuration Manager.
- Exposing CMS (Flax) directory tree.
- Metadata manager to expose publication metadata e.g. ISSN etc.
- Configuration setting to allow Group Managers/Editors to edit submitted reviews.




