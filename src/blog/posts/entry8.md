---
published: true
title: "Publishing and Facilitation"
date: 2022-02-17
intro: Adam discusses managing publishing processes in a concurrent environment.
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Publishing and Facilitation" src="https://peertube.coko.foundation/videos/embed/8feccf51-4424-40fd-b7df-3a52b877856d" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/facilitation.png"
---
