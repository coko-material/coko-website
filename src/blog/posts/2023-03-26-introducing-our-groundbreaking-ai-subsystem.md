---
title: "Here Come the Robots! : Introducing Our Groundbreaking AI Subsystem"
published: true
date: 2023-03-26
intro: Stay ahead of the game with Coko's cutting-edge AI technology. Our new
  subsystem in CokoDocs, Kotahi, and Ketida is designed to empower publishing
  partners and revolutionise their workflows.
icon: /static/images/uploads/adaminski_a_captivating_black_and_white_documentary_photo_of_a__f043bb04-05f3-4eee-a96d-574d71fd1a3a.png
image: /static/images/uploads/adaminski_a_captivating_black_and_white_documentary_photo_of_a__f043bb04-05f3-4eee-a96d-574d71fd1a3a.png
---
We are excited to share the launch of the Kotahi AI subsystem tailored for supporting publishing workflows. This subsystem will empower our journal partners to build AI features into their workflows and enable us to include AI capabilities in their publishing processes. Partners will now be able to leverage the power of AI to streamline their workflows, automate tasks, and improve productivity.

Our subsystem is built with existing machine learning algorithms and designed with the specific needs of our publishing partners in mind. We're eager to see how our partners will use it to drive their publishing businesses forward.  It offers a wide range of possibilities for partners, including automated image recognition and tagging, natural language processing for text analysis, predictive analytics for forecasting and trend analysis, and personalised recommendations based on user behavior. These features are just the beginning, and we're excited to see the innovative ways that our publishing partners will use the subsystem to enhance their products and services.

At Coko, we're committed to providing our publishing partners with the best possible experience. Our support for our new AI subsystem is a significant step forward in achieving that goal, and we're thrilled to be able to offer this technology to our publishing partners.

We're grateful to our community (particularly Amnet and eLife) for their support and feedback, which has been instrumental in its development. We look forward to continuing to work to refine and improve it to better serve  publishing needs.
