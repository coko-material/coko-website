---
title: Ketida Textbook Designer!
date: 2023-05-22
published: true
intro: |-
  Beautiful textbooks are here! Watch this amazing demo.

  [](https://ketida.community/)
video: ""
icon: /static/images/uploads/8dd6354e-fcbb-c21c-7979-a969b903fb7f.png
image: /static/images/uploads/8dd6354e-fcbb-c21c-7979-a969b903fb7f.png
---
<https://ketida.community/>\
\
Host Karen Lauritsen of the Open Education Network is joined by Coko's Christina Tromp (Ketida project manager), to review and discuss the Open Textbook Planner and Ketida publishing tools.\
\
It is pretty amazing. The textbooks produced are very beautiful, watch until the end!

<iframe width="560" height="315" src="https://www.youtube.com/embed/GpdQfAf1gtQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
