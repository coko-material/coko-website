---
published: true
title: Open Research Europe Report Praises Kotahi as Transformative Platform for
  Scholarly Publishing
date: 2023-12-07
intro: >
  A new report released this week from Open Research Europe (ORE) provides a
  glowing review of Kotahi, finding it to be "a cutting-edge platform for
  scholarly publishing that can accommodate various end-to-end publishing
  workflows." 
icon: /static/images/uploads/Kotahi.png
image: /static/images/uploads/screenshot-from-2023-12-07-13-53-42.png
---
A new report released this week from Open Research Europe (ORE) provides a glowing review of Kotahi, finding it to be "a cutting-edge platform for scholarly publishing that can accommodate various end-to-end publishing workflows." Developed by Coko, Kotahi aims to "promote open access to research articles."

The ORE report highlights Kotahi's innovations in streamlining scholarly publishing through advanced features for manuscript creation, editing, submission and peer review. As the report states, Kotahi "offers an easy and dynamic adaptation of workflows such as article submission and peer review" and implements "sophisticated peer-review flows, including those supporting the preprint model."

Additional features noted in the report include Kotahi's capabilities for assisting article authoring through "tools like track changes, math, images, captions, grammar checks etc." And with the internal CMS set to enable advanced discovery, the report finds Kotahi well-positioned as a transformative force in scholarly communication.

While acknowledging Kotahi's currently small user base, the ORE report still determines it to be an exciting development for large-scale publishing activities needing robust manuscript and peer review management. As Kotahi continues expanding its features and reach, the analysis sees strong potential to further enable open access, collaboration, and evolution in scholarly publishing.

The report was written when Kotahi was at version 1.5. We are now at version 2.2.

The full report can be found here:  [https://op.europa.eu/en/publication-detail/-/publication/cc087fd8-82b3-11ee-99ba-01aa75ed71a1/language-en](https://op.europa.eu/en/publication-detail/-/publication/cc087fd8-82b3-11ee-99ba-01aa75ed71a1/language-en)

For more information, please visit https://kotahi.community or contact adam@coko.foundation. 
