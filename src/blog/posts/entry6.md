---
published: true
title: "How to Radically Optimise Publishing"
date: 2022-02-11
intro: Adam discusses a key ingredient for radically improving the time and cost to publish.
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="How to radically optimise publishing" src="https://peertube.coko.foundation/videos/embed/15556ac8-d96b-4f42-8e06-bbd388af96f4" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/radically.png"
---
