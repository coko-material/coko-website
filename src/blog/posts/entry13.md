---
title: "Thoughts on ScienceBeam and Libero Editor"
published: true
date: 2022-04-04
intro: Thoughts about the exciting recent eLife Announcement.
author: Adam Hyde
tags: featured
class:
image: ""
video: <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Thoughts about ScienceBeam and Libero Editor" src="https://peertube.coko.foundation/videos/embed/7a032037-64b0-41b4-a53f-3847e1f072e5" frameborder="0" allowfullscreen></iframe>
icon: "/images/uploads/sb-le.png"
---
See also [https://elifesciences.org/inside-elife/daf1b699/elife-latest-announcing-a-new-technology-direction](https://elifesciences.org/inside-elife/daf1b699/elife-latest-announcing-a-new-technology-direction)
