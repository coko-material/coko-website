---
published: true
title: "CokoDocs coming soon!"
date: 2022-06-04
intro: New Product Announcement
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/CokoDocs.png"
---

![Coko Website](/images/uploads/CokoDocs.png)

We are happy to annouce a new product in the pipeline - CokoDocs. We have always wanted to use all the Coko components to build a general purpose open source replacement for Google Docs and similar products. Now, finally, we have some bandwidth and support to do so. CokoDocs will be a very simple, yet powerful and elegant, open source document manager. The product will have three main components - login, a dashboard, and an editor (wax - https://waxjs.net). 

In addition we will be bringing concurrent editing and document sharing to CokoDocs. 

The aim is to make CokoDocs a modern and easy to use general purpose online word processing environment.

In the next weeks and months we will announce some interesting partnerships to move this forward. If you would like to contribute to this effort then please email Coko Founder Adam Hyde - adam@coko.foundation 
