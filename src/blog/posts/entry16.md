---
published: true
title: "Coko Newsletter number 17 out now!"
date: 2022-05-14
intro: Featuring - Kotahi and JATS, Editoria new feature, more on the forthcoming PageBreak Conference.
author: Adam Hyde
tags: featured
video: ""
class:
icon: "/images/uploads/Coko.png"
---

![Coko Website](/images/uploads/Coko.png)

New Coko Newsletter out! #17 - Featuring Kotahi and JATS, new feature to Editoria, and more information about the forthcoming PageBreak conference. Read and/or subscribe here:  [https://mailchi.mp/563a109bab82/coko-newsletter-6815309](https://mailchi.mp/563a109bab82/coko-newsletter-6815309).
