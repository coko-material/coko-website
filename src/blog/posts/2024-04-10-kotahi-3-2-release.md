---
published: true
title: Kotahi 3.2 Release!
date: 2024-04-09
intro: >
  Less than one month after the release of Kotahi 3.1 we have Kotahi 3.2! Read
  on to learn more…
icon: /static/images/uploads/edit_modalsm.png
image: /static/images/uploads/edit_modalsm.png
---
Less than one month after the release of Kotahi 3.1 we have Kotahi 3.2! Read on to learn more…

### Downloads

- [Source code 3.2.0 (zip)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/3.2.0/kotahi-3.2.0.zip)
- [Source code 3.2.0 ([tar.gz](tar.gz))](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/3.2.0/kotahi-3.2.0.tar.gz)
- [Source code 3.2.0 (tar.bz2)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/3.2.0/kotahi-3.2.0.tar.bz2)
- [Source code 3.2.0 (tar)](https://gitlab.coko.foundation/kotahi/kotahi/-/archive/3.2.0/kotahi-3.2.0.tar)

**AI Design Studio**;
added to the Production editor. With a focus on PDF production, utilize the studio to tweak page layouts, adjust image placements, manage widows and orphans, refine content with ease, or come up with completely new designs using the studio. Read more here; [[https://www.robotscooking.com/redefining-document-design-unveiling-our-ai-powered-pdf-designer/](https://www.robotscooking.com/redefining-document-design-unveiling-our-ai-powered-pdf-designer/)](https://www.robotscooking.com/redefining-document-design-unveiling-our-ai-powered-pdf-designer/)

![](/static/images/uploads/ai-design-studio-logo-colors.png)
Select an area (element) on the screen and insert a prompt into the  AI chat editor and see the result! Add your OpenAI credentials on the Configuration>Integrations and Publishing Endpoints>OpenAI access key to activate the service.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/b537efeea1e5364368db1e5187087a2e/ai_design_studio.png)

**Citation parser enhancements**; make it easier for Production editors to edit and apply changes to citation data captured in the edit modal. The ‘Original’ citation is now displayed above the ‘Edited’ version - form field changes will now reflect in the ‘Edited’ display, in real-time.

The citation style is now displayed in the modal as applied in the Configuration>Production>‘Select style formatting for citations’ field.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/7a684e9dbb5e339a883c9c39c3550a65/apply_modal.png)

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/cbbfb524f0864588097fd80ab6c6fcb9/edit_modal.png)

**The Configuration page**; has been reorganized to improve usability. We have now listed settings within  collections;

- ‘General’; instance type and group (brand) identity.
- ‘Workflow’; Dashboard, Manuscripts page, Control panel, Submission, Review page, Task Manager and Reports.
- ‘Production’; Citation parser settings.
- ‘Integration and Publishing Endpoints’; Semantic Scholar, Hypothesis, Crossref, Webhooks, Kotahi APIS, COAR notify, AI Design Studio key.
- ‘Notifications and E-mails’;  configure your inbox and Event notifications.

![](https://gitlab.coko.foundation/kotahi/kotahi/uploads/1019cff21c617ad5628f2bda66f24fbc/config_page.png)

**Sticky chat**; currently, you can expand/collapse chat. However, the state is not retained between sessions or when navigating between pages. We now preserve the state of the chat so that if a user expands/collapses a chat window that state is retained if they navigate away and come back to the page.

**Other enhancements**;

- Manuscript metadata is now indexed - keyword search will now display results based on data captured in the Submission form.
- Resolved a few more edge cases and improved the conversion of MathType equations for editing in Wax.
- Dashboard name changes; “My Submissions” (no change), “To  Review”->‘Review Assignments’ and “Manuscripts I’m Editor of”->‘Editing Queue’.
- We have created a tabbed view for the Configuration page.

**Bug fixes**;

- Fixed an email notification variable ([Handlebars.js](Handlebars.js)) data mapping issue.
- Reviewers should only be able to be invited manually or using the Reviewer invitation.
- Fix for unsubmitted review data that was not being saved on blur.
- Invited author assigned as reviewer.
- Review page access is revoked when a new version is submitted.
- Fix for the translation overwrite file that was not being fully applied.
- Fix ‘You have unsaved changes’ from appearing on altering the front-end schema of the Configuration manager.
- When a user is invited to participate in a review using the ‘Review invitation’ email notification - the review is now also listed in Dashboard> Review Assignments.

**Next release**;

- Exposing CMS (Flax) directory tree.
- Metadata manager to expose publication metadata e.g. ISSN etc.
- Support for adding collections to Flax e.g. articles to a journal issue.
- Configuration setting to allow Group Managers/Editors to edit submitted reviews.
- New `lab` archetype that supports document collaboration.




