---
published: true
title: "AI Assistance Arrives on Ketty: A New Era for Efficient Book Production"
date: 2024-03-04
intro: >
  We've recently enhanced the
  [Ketty](https://ketty.community/?ref=robotscooking.com) book production
  platform, which is 100% open source, by integrating an AI Assistant. 
video: https://vimeo.com/919014318
icon: /static/images/uploads/ketty.png
---
This integration aims to streamline the workflow by allowing users to 
generate and work with AI-produced content directly within Ketty, 
eliminating the need to alternate between a separate LLM chat window and
 the book project. More information in Coko Newsletter #47 [https://mailchi.mp/2817d00c309a/coko-newsletter-8307468](https://mailchi.mp/2817d00c309a/coko-newsletter-8307468)
