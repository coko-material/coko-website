---
title: Manifesto 
introduction:
menu: true
class: manifesto 
layout: single.njk
order: 7
heading: A Manifesto for Transforming Publishing
chapeau: "It’s time for a change"
---

Publishing is stuck. Legacy organizations cling to outdated models, employing rigid processes on antiquated software. This results in expensive, sluggish, and wasteful systems that stifle innovation and better ways of sharing knowledge. Dominant structures and inflexible conventions have stalled attempts to improve publishing, as core power centers remain entrenched, and change remains superficial.

Yet, while the current implementation is broken, the goal of publishing—creating, refining, and sharing knowledge—remains vital. We must reconnect with this core purpose and rethink how we achieve it.

## The Path Forward

The journey begins with re-envisioning publishing from its foundations. At Coko, we focus on developing the essential building blocks that enable this transformation. We are constructing core open infrastructure, tools, and platforms aligned with publishing’s true purpose: advancing collective knowledge. With this open infrastructure in place, pioneering new publishing models become possible.

From this position of collective power, we envision a limitless future for publishing, where bold new models emerge through open collaboration. A future transformed by radical openness, where minds connect across organizations to co-create, innovate, and progress together.

## An Open Ecosystem

In this open ecosystem, cross-functional teams seamlessly collaborate—driven by needs and human-centric technologies, not legacy conventions and obsolete systems. The boundaries between creators and publishers dissolve, merging together in the service of knowledge creation, not antiquated organizational structures.

Through open collaboration, knowledge becomes more accessible, more representative, and more meaningful. Timelines accelerate, costs plummet, and errors disappear as capabilities expand continuously.

# Core Values and Vision

Guided by our core values of trust, openness, and collective empowerment, we must reimagine publishing from the ground up together. Our shared responsibility is to pioneer new workflows, technologies, and models that unlock publishing’s immense collaborative potential—not simply improving publishing, but redefining it.

## Embracing Open Web Formats

We advocate for the adoption of open web formats throughout the publishing workflow. Open web formats (e.g., HTML, JSON, CSV) should be embraced as core publishing formats. They enable publishing on the world’s largest collaborative platform—the web.

## Advocating Single Source Systems

By using one content source across authoring and production, Single Source systems enable seamless collaboration, reduce handoffs, eliminate conversions, condense timelines, and connect workflows.

## Simplifying Publishing

The current fragmented systems result in disconnected teams, wasted effort, and preventable errors. Publishing should focus on needs, not be constrained by existing organizational structures. The boundaries between authors and publishers must dissolve, embracing collaborative creation and dissemination.

## Innovative Technologies for New Processes

To reimagine publishing, innovative workflows require empowering new technologies. Collaboration, concurrency, and communication are essential. By integrating real-time teamwork into systems, we can break down silos, work in parallel, condense timelines, and reduce costs.

## Deep Collaboration and Facilitative Leadership

Deep collaboration transforms structures, flattening hierarchies, decentralizing leadership, and shifting to collective ownership and integrated teamwork. Facilitative leadership unlocks collective potential, replacing dominance with inclusion, silos with openness, and individual agendas with shared purpose. The result is empowered teams, accelerated workflows, and publishing that truly serves its community.

## Designing Open, Cross-Functional Spaces

Cross-functional digital spaces allow teams to collaborate without boundaries. Hardcoded workflows create gated paths that inhibit emergent teamwork. Open Source tools reduce the barrier to entry for innovators, providing high-quality, innovative systems at lower costs by sharing efforts across organizations.

# Join Us in Redefining Publishing

Together, we can transform publishing into a more collaborative, efficient, and innovative field. Let’s reimagine the future of knowledge sharing and build a better tomorrow.

*This is publishing’s collaborative future. This is Coko’s journey.*

