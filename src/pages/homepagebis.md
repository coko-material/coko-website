---
title: Transforming Publishing
permalink: /index.html
layout: home-alt.njk
class: home
heading: "Innovating and modernizing publishing with open-source, customizable platforms that let you own your infrastructure, streamline workflows, support emergent models, and enhance global collaboration."
partnersImage: logos-7.png
chapeau: "Innovating and modernizing publishing with open-source, customizable platforms that let you own your infrastructure, streamline workflows, support emergent models, and enhance global collaboration."
platforms:
  - logo: "/images/homelogo/nomargins/kotahi.svg"
    name: "Kotahi"
    baseline: The Next Generation of Scholarly Publishing Platform
    description: One flexible, customisable platform for journals, preprint servers, review communities, and micropublications.
    usedby: As used by eLife, Johns Hopkins University, Amnet, Biophysics Colab and more..
    weburl: "https://kotahi.community"

  - logo: "/images/homelogo/nomargins/ketty.svg"
    name: "Ketty"
    baseline: Revolutionizing Book Production
    description: Streamlined, scalable professional book production platform.
    usedby: Supported by Mellon. Used by Lulu, Open Education Network, Amnet, BookSprints, WACREN and more...
    weburl: "https://ketty.community"

  - name: HHMI Assesment Builder
    logo: /images/homelogo/nomargins/hhmi-ab.svg
    baseline: Modern, Configurable, Learning Assessment Builder
    description: HHMI Assessment Builder is an end-to-end question authoring, peer-review, and publishing platform for faculty to curate assessments, for use in Learning Management Systems and the classroom.
    usedby: Commissioned by, and built for, HHMI BioInteractive
    weburl: "https://gitlab.coko.foundation/hhmi/hhmi"

  - logo: /images/homelogo/nomargins/datacite.svg
    name: Citation Visualisation Platform
    baseline: Citation Visualisation Platform
    description: Custom-built open platform to interactively navigate and analyse DataCite's 6 million+ citation corpus.
    usedby: Built by Coko for DataCite and Chan Zuckerberg Initiative (CZI). Commissioned by Wellcome Trust
    weburl: "https://makedatacount.org/first-release-of-the-open-global-data-citation-corpus/"

  - logo: /images/uploads/bcms2.png
    name: ncbi
    baseline: Book CMS (BCMS) for BioMedical Research
    description: A multi-organisational, multiformat book aggregation, conversion, QA and publication platform.
    usedby: Commissioned by, and built for, NCBI
    weburl: "https://gitlab.coko.foundation/ncbi/ncbi"

  - logo: "/images/homelogo/nomargins/micropub.svg"
    name: "microPublication.org"
    baseline: Submission, peer review and publication platform for micropublications.
    usedby: Commissioned by Caltech and used by WormBase and microPublication.org
    weburl: "https://www.micropublication.org/"

infrastructure:
  - logo: "/images/homelogo/nomargins/pagedjs.svg"
    name: paged.js
    baseline: Typesetting Engine for Creating PDF from HTML
    description: Paged.js paginates HTML content to PDF in the browser, enabling design of print publications with web tools.
    usedby: As used by Musée du Louvre, C&F Editions, Pagedown, Ketty, Kotahi, Lulu, Hederis
    weburl: https://pagedjs.org

  - logo: "/images/homelogo/nomargins/flax.svg"
    name: "Flax"
    baseline: Coko’s Open Source CMS
    description: Flax is a publishing front end, a web presence for content produced in Kotahi and Ketty.
    usedby: As used by Musée du Louvre, Book Sprints, and as internal CMS for Kotahi.
    weburl: "https://gitlab.coko.foundation/flax-development-space/"

  - logo: "/images/homelogo/nomargins/xsweet.svg"
    name: XSweet
    baseline: High Fidelity MS Word to HTML Converter
    description: XSweet extracts MS Word content into HTML.
    usedby: As used by Ketty, Kotahi
    weburl: https://xsweet.org/

  - logo: "/images/homelogo/nomargins/cokoserver.svg"
    weburl: "https://gitlab.coko.foundation/cokoapps/server"
    name: CokoServer
    baseline: Framework for Building Publishing Platforms
    description: An Open Source framework for building modern publishing platforms.
    usedby: As used by all Coko products, EPMC, Hindawi Phenom, Libero Editor and other community products

  - logo: "/images/homelogo/nomargins/liberoeditor.svg"
    name: "Libero Editor"
    baseline: JATS XML Editor
    description: Libreo Editor Enables editing and structuring of JATS XML documents, originally built by eLife.
    weburl: "https://gitlab.coko.foundation/libero/"

  - logo: "/images/homelogo/nomargins/sciencebeam.svg"
    name: "Science Beam"
    baseline: Machine Learning PDF to XML
    description: ScienceBeam uses Machine Learning for accurate PDF to XML conversion, originally built by eLife
    weburl: "https://gitlab.coko.foundation/sciencebeam"
---

Innovating and modernizing publishing with open-source, customizable platforms that let you own your infrastructure, streamline workflows, support emergent models, and enhance global collaboration. [Collaborate with us](mailto:adam@coko.foundation).
