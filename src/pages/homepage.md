---
title: Coko
permalink: /index-alt.html
layout: home.njk
class: home
heading: Transforming Publishing.
partnersImage: logos-7.png
chapeau: "We build ready-to-use publishing platforms for the community and custom-built solutions for you."
products:
  - logo: "/images/uploads/Kotahi.png"
    name: "Kotahi"
    baseline: The Next Generation of Scholarly Publishing Platform
    description: One flexible, customisable platform for journals, preprint servers, review communities, and micropublications.
    usedby: As used by eLife, Johns Hopkins University, Amnet, biophysics COLAB
    weburl: "https://kotahi.community"

  - logo: "/images/press/ketty.svg"
    name: "Ketty"
    baseline: Revolutionizing Book Production
    description: Build and customize streamlined, scalable professional book production workflows using Ketty’s rich web-based tools.
    usedby: As used by Lulu, Open Education Network, Amnet, BookSprints, WACREN
    weburl: "https://ketty.community"

  - logo: "/images/press/aid.png"
    name: "AI Design Studio"
    baseline: AI Driven Design Studio for multiple formats
    description: Powerful single source design tool.
    usedby: Kotahi, Ketty.
    weburl: WWW coming soon...

  - logo: "/images/uploads/Micropublication.png"
    name: "microPublication.org"
    baseline: Submission, peer review and publication platform for micropublications.
    usedby: Commissioned by caltech and used by wormbase and micropublication.org
    weburl: "https://www.micropublication.org/"

  - name: hhmi Assesment Builder
    logo: /images/uploads/HHMI.png
    baseline: Modern, Configurable, Learning Assessment Builder
    description: End to end question authoring tool with peer review, question curation, LMS export and publication interface for faculty and students.
    usedby: Commissioned by HHMI Biointeractive

  - logo: /images/uploads/NCBI.jpg
    name: ncbi
    baseline: Large Scale Book Content Management System.
    description: A multi-organisational, multiformat book aggregation, conversion, QA and publication platform.
    usedby: Commissioned by NCBI
    weburl: MISSING

  - logo: /images/ctv.png
    name: Citation Visualisation Platform
    baseline: Citation Visualisation Platform
    description: Custom-built open platform to interactively navigate and analyse DataCite's 6 million+ citation corpus.
    usedby: Commissioned by Wellcome Trust

  - logo: "/images/uploads/Pagedjs.png"
    name: paged.js
    baseline: Create PDF Output from HTML Content
    description: Paged.js paginates HTML content to PDF in the browser, enabling design of print publications with web tools.
    usedby: As used by Musée du Louvre, C&F Editions, Pagedown, Ketty, Kotahi, Lulu, Hederis
    weburl: https://pagedjs.org

  - logo: "/images/uploads/flax.png"
    name: "Flax"
    baseline: Coko’s open source CMS
    description: Flax is a publishing front end, a web presence for content produced in Kotahi and Ketty.
    usedby: As used by Musée du Louvre, Book Sprints, and as internal CMS for Kotahi.
    weburl: "https://gitlab.coko.foundation/flax-development-space/"

  - logo: "/images/uploads/Wax.png"
    name: "Wax"
    baseline: Configurable Web-Based Word Processor
    description: Wax is a customizable online word processor with plugins for various formatting and document needs.
    usedby: As used by all Coko platforms and bespoke solutions
    weburl: "https://waxjs.net"

  - logo: "/images/uploads/xSweet.png"
    name: XSweet
    baseline: High Fidelity docx to HTML Converter
    description: XSweet extracts Word docx content into editable HTML for publishing, importing, or converting further.
    usedby: As used by Ketty, Kotahi
    weburl: https://xsweet.org/

  - logo: "/images/uploads/Pubsweet.png"
    name: "CokoServer"
    description: "CokoServer is Coko’s free, open source framework for building state-of-the-art publishing platforms. Built by the Coko team."
    weburl: "https://pubsweet.coko.foundation/"

  - logo: "/images/uploads/NCBI.jpg"
    name: "NCBI Bookshelf"
    description: "Built for the NCBI and will replace the current NCBI Bookshelf CMS. It is an extensive project with a lot of internal integrations and migrations. Built by the Coko team."
    weburl: ""

  - logo: "/images/uploads/HHMI.png"
    name: HHMI Assessment Builder.
    baseline: Modern, Configurable, Learning Assessment Builder
    description: End to end question authoring tool with peer review, question curation, LMS export and publication interface for faculty and students.
    usedby: Commissioned by HHMI Biointeractive
    weburl: MISSING

  - logo: /images/upload/cokoserver.png
    name: CokoServer
    baseline: Framework for Publishing Platforms
    description: Provides an open source framework for building modern publishing platforms.
    usedby: As used by all Coko products, EPMC, Hindawi Phenom, Libero Editor and other community products
    weburl: MISSING

  - logo: "/images/uploads/liberoeditor.png"
    name: "Libero Editor"
    baseline: JATS XML Editor
    description: Enables editing and structuring of JATS XML documents, originally built by eLife.
    weburl: MISSING

  - logo: /images/uploads/liberoreviewer.png
    name: Libero Reviewer
    baseline: Manuscript Submission and Peer Review
    description: A custom-built manuscript and peer review system for eLife, originally created by eLife and now maintained by Coko, incrementally upgrading their previous platform.
    weburl: MISSING
---

Through mission-driven partnerships, Coko architects highly customisable publishing platforms, bespoke solutions and workflow innovations that drive open access, catalyze innovation, reduce costs and pioneer new models—co-creating infrastructure for the future.

Explore Coko innovations and [connect with us](mailto:adam@coko.foundation) to collaborate.
