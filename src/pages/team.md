---
title: People
menu: true
layout: team.njk
class: team
order: 10 
heading: The most experienced team in publishing.
---

## We’ve built over a dozen highly successful publishing platforms – making us the most experienced folks in publishing.

<img src="/images/uploads/Coko-team-map.svg" class="team-map" alt="Coko-team-map">
