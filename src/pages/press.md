---
title: Logos and comms
menu: false
layout: single.njk
class: press
heading: Logos and press
---

## Coko’s brand 
Coko’s logo is available with or without the baseline.
You can download them as SVG, JPG or PNG with transparency depending on your needs.

[![Coko Logo color with baseline](/images/press/coko-color-baseline.svg)](/images/press/coko-color-baseline.svg)

Download as [PNG](/images/press/coko-color-baseline.png) – [JPG](/images/press/coko-color-baseline.jpg) – [SVG](/images/press/coko-color-baseline.svg)


[![Coko Logo black with baseline](/images/press/coko-black-baseline.svg)](/images/press/coko-black-baseline.svg)

Download as [PNG](/images/press/coko-black-baseline.png) – [JPG](/images/press/coko-black-baseline.jpg) – [SVG](/images/press/coko-black-baseline.svg)

[![coko logo colors callout](/images/press/coko-callout-color.svg)](/images/press/coko-callout-color.svg)

Download as [PNG](/images/press/coko-callout-color.png) – [JPG](/images/press/coko-callout-color.jpg) – [SVG](/images/press/coko-callout-color.svg)

[![coko logo black callout](/images/press/coko-callout-black.svg)](/images/press/coko-callout-black.svg)

Download as [PNG](/images/press/coko-callout-black.png) – [JPG](/images/press/coko-callout-black.jpg) – [SVG](/images/press/coko-callout-black.svg)

White: 

Download as [PNG](/images/press/coko-callout-white.png) – [JPG](/images/press/coko-callout-white.jpeg) – [SVG](/images/press/coko-callout-white.svg)

[![coko logo horizontal color](/images/press/coko-horizontal-color.svg)](/images/press/coko-horizontal-color.svg)

Download as [PNG](/images/press/coko-horizontal-color.png) – [JPG](/images/press/coko-horizontal-color.jpeg) – [SVG](/images/press/coko-horizontal-color.svg)

[![coko logo horizontal black](/images/press/coko-horizontal-black.svg)](/images/press/coko-horizontal-black.svg)

Download as [PNG](/images/press/coko-horizontal-black.png) – [JPG](/images/press/coko-horizontal-black.jpeg) – [SVG](/images/press/coko-horizontal-black.svg)

White:  

Download as [PNG](/images/press/coko-horizontal-white.png) – [JPG](/images/press/coko-horizontal-white.jpeg) – [SVG](/images/press/coko-horizontal-white.svg)
 
[![Coko Logo color vertical](/images/press/coko-vertical-color.svg)](/images/press/coko-vertical-color.svg)

Download as [PNG](/images/press/coko-vertical-color.png) – [JPG](/images/press/coko-vertical-color.jpeg) – [SVG](/images/press/coko-vertical-color.svg)

[![Coko Logo black vertial](/images/press/coko-vertical-black.svg)](/images/press/coko-vertical-black.svg)

Download as [PNG](/images/press/coko-vertical-black.png) – [JPG](/images/press/coko-vertical-black.jpeg) – [SVG](/images/press/coko-vertical-black.svg)

White:

Download as [PNG](/images/press/coko-vertical-white.png) – [JPG](/images/press/coko-vertical-white.jpeg) – [SVG](/images/press/coko-vertical-white.svg)

## Coko’s product

### Pubsweet

[![pubsweet logo](/images/press/pubsweet.svg)](/images/press/pubsweet.svg)

Download as [PNG](/images/press/pubsweet.png) – [JPG](/images/press/pubsweet.jpg) – [SVG](/images/press/pubsweet.svg)

### Ketty

[![ketty logo](/images/press/ketty.svg)](/images/press/ketty.svg)

<!-- Download as [PNG](/images/press/ketty.png) – [JPG](/images/press/ketty.jpg) – [SVG](/images/press/ketty.svg) -->

### Kotahi

[![kotahi logo](/images/press/kotahi.svg)](/images/press/kotahi.svg)

Download as [PNG](/images/press/kotahi.png) – [JPG](/images/press/kotahi.jpg) – [SVG](/images/press/kotahi.svg)

### Cokodocs

[![cokodocs logo](/images/press/cokodocs.svg)](/images/press/cokodocs.svg)

Download as [PNG](/images/press/cokodocs.png) – [JPG](/images/press/cokodocs.jpg) – [SVG](/images/press/cokodocs.svg)

### Paged.js

[![pagedjs logo](/images/press/pagedjs.svg)](/images/press/pagedjs.svg)

Download as [PNG](/images/press/pagedjs.png) – [JPG](/images/press/pagedjs.jpg) – [SVG](/images/press/pagedjs.svg)

### Wax

[![wax color logo](/images/press/wax-color.svg)](/images/press/wax-color.svg)

Download as [PNG](/images/press/wax-color.png) – [JPG](/images/press/wax-color.jpg) – [SVG](/images/press/wax-color.svg)

[![wax black logo](/images/press/wax-black.svg)](/images/press/wax-black.svg)

Download as [PNG](/images/press/wax-black.png) – [JPG](/images/press/wax-black.jpg) – [SVG](/images/press/wax-black.svg)

### Xsweet 

[![Xsweet logo](/images/press/xsweet-color.svg)](/images/press/xsweet-color.svg)

Download as [PNG](/images/press/xsweet-color.png) – [JPG](/images/press/xsweet-color.jpg) – [SVG](/images/press/xsweet-color.svg)

### Flax


[![Flax logo](/images/press/flax.svg)](/images/press/flax.svg)

Download as [PNG](/images/press/flax.png) – [JPG](/images/press/flax.jpg) – [SVG](/images/press/flax.svg)

