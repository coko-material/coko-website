---
title: About
introduction:
menu: true
class: about
layout: single.njk
order: 1
heading: Transforming Publishing
chapeau: "We design and build systems that are truly transformational."
---
At the heart of our mission is the transformation of the publishing landscape. We design and build systems that are truly transformational, benefiting the publishing community with modern, open-source tools that enable the dissemination of critical knowledge better, faster, and cheaper.

### Our Mission and Economic Model

Our economic model is straightforward. Organizations hire us to build and extend open-source software, and we reinvest the surplus to create open-source software for the broader community. It's a bit like a modern Robin Hood approach to software development, channeling resources to serve the greater good.

### Beyond Tools: The Coko Community

We are not just about building tools; we care deeply about the people who create and use these tools. This commitment is embodied in the Coko Community. We lead initiatives such as the Open Publishing Awards and the Open Publishing Fest, fostering a spirit of collaboration and innovation.

## Our Core Beliefs

### Collaboration and Community

Collaboration and community are at the core of everything we do. These principles define who we are, how we work, and what we believe in—openness, transparency, and respect for all.

### Transformative Workflows

We believe that better workflows will revolutionize publishing. This single-minded focus drives all our efforts.

### Open Source as the Future

We are convinced that open source is the future of publishing tools. Sharing source code and our learnings is the only way forward.

### Real-World Solutions

Our products are designed to solve real problems and simplify the lives of those doing the actual work.

### Challenging the Status Quo

We don't shy away from asking tough questions and challenging existing assumptions. This critical approach helps drive meaningful change.

### Not-for-Profit Commitment

Coko operates as a fiscally sponsored charitable not-for-profit project. All surplus funds are reinvested into developing what we call “Community Products.” These products are developed with the community, for the community, and are freely available as open-source solutions.

Coko is dedicated to working exclusively with organizations committed to developing 100% open-source products. Our goal is to build a collaborative, open, and innovative future for publishing.
