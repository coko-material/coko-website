---
title: Community
menu: true
class: community
layout: single.njk
order: 5 
heading: 100% Community.
chapeau: ""
---

<!-- ## We are experts in publishing technology and development. We help established publishers improve existing workflows and new entrants explore new ways to publish. Our services include: -->
## Coko Commons


-  
  ![](/images/3-milion-graphics.png)
  The Coko Commons is a vast pool of open-source code designed to transform publishing.  We leverage this shared resource to build products for the benefit of the entire community.

## Community Products


- 
  ![](/images/1-milion-graphics.png)
  All softwares Coko produces are Open Source. For many of these products we also facilitate large communities to collaborate on design and development - we call these "Community Products". A short list of Coko Community Products includes Kotahi, Ketty, PagedJS, AI Design Studio, Wax, and XSweet.

## Open Publishing Fest


- 
  ![](/images/opf.svg)
  Together, we find new ground to share our ideas - The Open Publishing Fest celebrates communities developing open creative, scholarly, technological, and civic publishing projects. This is a collaborative, distributed event. Sessions are hosted by individuals and organizations around the world as panel discussions, fireside chats, demonstrations, performances etc. 

  [openpublishingfest.org](https://openpublishingfest.org/)

## Open Publishing Awards

-  
  ![](/images/opa.svg)
  Celebrating the value of open in publishing -
  The annual Open Publishing Awards celebrate publishing software and content that use open licenses. Hosted every two years. 

  [openpublishingawards.org](https://openpublishingawards.org/)

